package org.silk.slack.coin.event;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.regex.Matcher;


class MessageEventHandlerTest {

  @Test
  void testPattern() {
    Matcher m = MessageEventHandler.MONEY_PATTERN.matcher("Toto 20 :money_with_wings::money_with_wings:");
    Assertions.assertThat(m.matches()).isTrue();
    Assertions.assertThat(m.groupCount()).isEqualTo(3);
    Assertions.assertThat(m.group(1)).isEqualTo("20");
    Assertions.assertThat(m.group(2)).isEqualTo(" :money_with_wings::money_with_wings:");
    Assertions.assertThat(m.group(3)).isEqualTo(":money_with_wings:");


    m = MessageEventHandler.MONEY_PATTERN.matcher("Toto :money_with_wings:");
    Assertions.assertThat(m.matches()).isTrue();
    Assertions.assertThat(m.groupCount()).isEqualTo(3);
    Assertions.assertThat(m.group(1)).isEqualTo("");
    Assertions.assertThat(m.group(2)).isEqualTo(" :money_with_wings:");
    Assertions.assertThat(m.group(3)).isEqualTo(" :money_with_wings:");
  }
}

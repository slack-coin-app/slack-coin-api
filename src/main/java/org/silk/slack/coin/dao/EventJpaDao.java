package org.silk.slack.coin.dao;

import org.silk.slack.coin.dao.entity.Event;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.stereotype.Repository;

@Repository
public interface EventJpaDao  extends JpaRepositoryImplementation<Event, String> {
}

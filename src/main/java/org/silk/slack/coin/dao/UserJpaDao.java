package org.silk.slack.coin.dao;

import java.util.List;
import java.util.Optional;
import org.silk.slack.coin.dao.entity.UserCached;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.stereotype.Repository;

@Repository
public interface UserJpaDao extends JpaRepositoryImplementation<UserCached, Long> {

  Optional<UserCached> findByTeamIdAndUserId(String teamId, String userId);

  List<UserCached> findAllByTeamId(String teamId);
}

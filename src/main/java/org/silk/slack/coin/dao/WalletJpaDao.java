package org.silk.slack.coin.dao;

import java.util.Collection;
import java.util.Optional;
import javax.persistence.LockModeType;
import org.silk.slack.coin.dao.entity.UserCached;
import org.silk.slack.coin.dao.entity.Wallet;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface WalletJpaDao extends JpaRepositoryImplementation<Wallet, Long> {
  @Lock(value = LockModeType.PESSIMISTIC_WRITE)
  Optional<Wallet> findAndLockByUserTeamIdAndUserUserId(String teamId, String userId);

  @Modifying(flushAutomatically = true)
  @Query("""
      update Wallet
      set receivedBalance = receivedBalance + :amount,
          version = version + 1
      where user in :users
      """)
  void giveCoin(@Param("users") Collection<UserCached> users,
      @Param("amount") long amount);

  @Modifying(flushAutomatically = true)
  @Query("""
      update Wallet
      set receivedBalance = 0,
          availableBalance = 0,
          givenBalance = 0,
          version = version + 1
      where user.id in (select id
          from UserCached
          where teamId = :teamId)
      """)
  void resetCoin(@Param("teamId") String teamId);

  @Modifying(flushAutomatically = true)
  @Query("""
      update Wallet
        set availableBalance = availableBalance + 1,
        version = version + 1
      where user.id in (select id
        from UserCached
        where eligible is true)
      """)
  int giveCoins();
}

package org.silk.slack.coin.event;

import com.slack.api.app_backend.events.payload.EventsApiPayload;
import com.slack.api.bolt.context.builtin.EventContext;
import com.slack.api.bolt.response.Response;
import com.slack.api.model.event.TeamJoinEvent;
import org.silk.slack.coin.command.SyncCommand;
import org.silk.slack.coin.dao.UserJpaDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class TeamJoinEventHandler implements EventHandler<TeamJoinEvent> {
  @Autowired
  private SyncCommand syncCommand;
  @Autowired
  private UserJpaDao userJpaDao;

  @Override
  public Class<TeamJoinEvent> applicableClass() {
    return TeamJoinEvent.class;
  }

  @Override
  @Transactional
  public Response apply(EventsApiPayload<TeamJoinEvent> event, EventContext ctx) {
    ctx.logger.info("New team join event received");
    var user = event.getEvent().getUser();
    var opt = userJpaDao.findByTeamIdAndUserId(event.getTeamId(), user.getId());
    if (opt.isEmpty()) {
      syncCommand.addUser(user, ctx);
    }
    return ctx.ack();
  }
}

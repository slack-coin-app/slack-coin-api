package org.silk.slack.coin.event;

import com.slack.api.app_backend.events.payload.EventsApiPayload;
import com.slack.api.bolt.context.builtin.EventContext;
import com.slack.api.bolt.response.Response;
import com.slack.api.model.event.UserChangeEvent;
import org.silk.slack.coin.command.SyncCommand;
import org.silk.slack.coin.dao.UserJpaDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class UserChangedEventHandler implements EventHandler<UserChangeEvent> {

  @Autowired
  private SyncCommand syncCommand;
  @Autowired
  private UserJpaDao userJpaDao;

  @Override
  public Class<UserChangeEvent> applicableClass() {
    return UserChangeEvent.class;
  }

  @Override
  @Transactional
  public Response apply(EventsApiPayload<UserChangeEvent> event, EventContext ctx) {
    ctx.logger.info("New user changed event received");
    var user = event.getEvent().getUser();
    if (event.getTeamId().equals(user.getTeamId())) {
      var opt = userJpaDao.findByTeamIdAndUserId(user.getTeamId(), user.getId());
      if (opt.isEmpty()) {
        syncCommand.addUser(user, ctx);
      } else {
        var cached = opt.get();
        var newEligibility = SyncCommand.isEligible(user);
        if (cached.isEligible() != newEligibility) {
          cached.setEligible(newEligibility);
          userJpaDao.save(cached);
          ctx.logger.info("User {}[{}] eligibility changed to {}", user.getName(), user.getId(), newEligibility);
        }
      }
    }
    return ctx.ack();
  }
}

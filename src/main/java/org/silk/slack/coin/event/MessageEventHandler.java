package org.silk.slack.coin.event;

import com.slack.api.app_backend.events.payload.EventsApiPayload;
import com.slack.api.bolt.context.builtin.EventContext;
import com.slack.api.bolt.response.Response;
import com.slack.api.methods.SlackApiException;
import com.slack.api.methods.request.reactions.ReactionsAddRequest;
import com.slack.api.methods.request.usergroups.users.UsergroupsUsersListRequest;
import com.slack.api.model.block.RichTextBlock;
import com.slack.api.model.block.element.RichTextSectionElement;
import com.slack.api.model.event.MessageEvent;
import java.io.IOException;
import java.util.HashSet;
import java.util.Optional;
import java.util.regex.Pattern;
import org.silk.slack.coin.dao.EventJpaDao;
import org.silk.slack.coin.dao.UserJpaDao;
import org.silk.slack.coin.dao.WalletJpaDao;
import org.silk.slack.coin.dao.entity.Event;
import org.silk.slack.coin.dao.entity.Wallet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class MessageEventHandler implements EventHandler<MessageEvent> {
  public static final String MONEY_EMOJI = ":money_with_wings:";
  final static Pattern MONEY_PATTERN = Pattern.compile(".*?(\\d*)((\\s*%s\\s*)+).*?".formatted(MONEY_EMOJI), Pattern.CASE_INSENSITIVE);

  @Autowired
  private EventJpaDao eventJpaDao;
  @Autowired
  private WalletJpaDao walletJpaDao;
  @Autowired
  private UserJpaDao userJpaDao;

  @Override
  public Class<MessageEvent> applicableClass() {
    return MessageEvent.class;
  }

  @Override
  @Transactional
  public Response apply(EventsApiPayload<MessageEvent> payload, EventContext ctx) throws IOException, SlackApiException {
    if (eventJpaDao.existsById(payload.getEventId())) {
      return ctx.ack();
    }
    ctx.logger.info("New message event received");
    var event = payload.getEvent();
    var text = event.getText();
    var client = ctx.client();
    var reactionEmoji = "x";
    var matcher = MONEY_PATTERN.matcher(text);
    if (matcher.find()) {
      var recipients = new HashSet<String>();
      for (var block : event.getBlocks()) {
        if (block instanceof RichTextBlock richTextBlock) {
          for (var element : richTextBlock.getElements()) {
            if (element instanceof RichTextSectionElement sections) {
              for (var section : sections.getElements()) {
                if (section instanceof RichTextSectionElement.User user) {
                  recipients.add(user.getUserId());
                }
                if (section instanceof RichTextSectionElement.UserGroup userGroup) {
                  var response = ctx.client().usergroupsUsersList(UsergroupsUsersListRequest.builder()
                      .includeDisabled(false)
                      .teamId(event.getTeam())
                      .usergroup(userGroup.getUsergroupId())
                      .build());
                  recipients.addAll(response.getUsers());
                }
              }
            }
          }
        }
      }
      long totalSpend = 1;
      if (!matcher.group(1).isBlank()) {
        totalSpend *= Long.parseLong(matcher.group(1));
      }
      totalSpend *= matcher.group(2).split(":money_with_wings:", -1).length - 1;

      recipients.remove(event.getUser()); // should not give to self
      if (!recipients.isEmpty()) {
        var eligibleUsers = userJpaDao.findAll((root, q, cb) -> cb.and(
            cb.isTrue(root.get("eligible")),
            cb.equal(root.get("teamId"), event.getTeam()),
            root.get("userId").in(recipients)
        ));
        long sum = totalSpend * eligibleUsers.size();
        ctx.logger.info("Transfer of {} to {} users from {}", sum, eligibleUsers.size(), event.getUser());
        Optional<Wallet> opt = walletJpaDao.findAndLockByUserTeamIdAndUserUserId(event.getTeam(), event.getUser());
        if (opt.isPresent()) {
          Wallet wallet = opt.get();

          if (wallet.getAvailableBalance() >= sum) {
            walletJpaDao.giveCoin(eligibleUsers, totalSpend);
            wallet.setAvailableBalance(wallet.getAvailableBalance() - sum);
            wallet.setGivenBalance(wallet.getGivenBalance() + sum);
            reactionEmoji = "heavy_check_mark";
          }
        }
      }
      var reaction = client.reactionsAdd(ReactionsAddRequest.builder()
          .channel(event.getChannel())
          .timestamp(event.getTs())
          .name(reactionEmoji)
          .build());
      if (!reaction.isOk()) {
        ctx.logger.error("reactions.add failed: {}", reaction.getError());
      }
    }
    eventJpaDao.save(new Event(payload.getEventId()));
    return ctx.ack();
  }

}

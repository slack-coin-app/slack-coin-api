package org.silk.slack.coin.event;

import com.slack.api.bolt.handler.BoltEventHandler;
import com.slack.api.model.event.Event;

public interface EventHandler<T extends Event> extends BoltEventHandler<T> {
  Class<T> applicableClass();
}

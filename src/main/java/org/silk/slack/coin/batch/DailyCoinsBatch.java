package org.silk.slack.coin.batch;

import lombok.extern.slf4j.Slf4j;
import org.silk.slack.coin.dao.WalletJpaDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Component
public class DailyCoinsBatch {
  @Autowired
  private WalletJpaDao walletJpaDao;

  @Scheduled(cron = "${job.daily-coins.cron}")
  @Transactional
  public void giveCoins() {
    var updated = walletJpaDao.giveCoins();
    log.info("{} coins given", updated);
  }
}

package org.silk.slack.coin.command;

import com.slack.api.bolt.context.Context;
import com.slack.api.bolt.context.builtin.SlashCommandContext;
import com.slack.api.bolt.handler.builtin.SlashCommandHandler;
import com.slack.api.bolt.request.builtin.SlashCommandRequest;
import com.slack.api.bolt.response.Response;
import com.slack.api.methods.SlackApiException;
import com.slack.api.methods.request.users.UsersInfoRequest;
import com.slack.api.methods.request.users.UsersListRequest;
import com.slack.api.model.User;
import java.io.IOException;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.silk.slack.coin.dao.UserJpaDao;
import org.silk.slack.coin.dao.WalletJpaDao;
import org.silk.slack.coin.dao.entity.UserCached;
import org.silk.slack.coin.dao.entity.Wallet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component("sync-team")
public class SyncCommand implements SlashCommandHandler {
  @Autowired
  private UserJpaDao userJpaDao;
  @Autowired
  private WalletJpaDao walletJpaDao;

  @Override
  @Transactional
  public Response apply(SlashCommandRequest request, SlashCommandContext ctx) throws SlackApiException, IOException {
    var eventUser = request.getPayload().getUserId();
    var slackUser = ctx.client().usersInfo(UsersInfoRequest.builder()
            .user(eventUser)
            .build());
    if (slackUser.getUser().isAdmin()) {
      var teamId = request.getPayload().getTeamId();
      var userList = ctx.client().usersList(UsersListRequest.builder()
              .teamId(teamId)
              .build());
      var usersCached = userJpaDao.findAllByTeamId(teamId)
              .stream()
              .collect(Collectors.toMap(UserCached::getUserId, Function.identity()));
      for (var user : userList.getMembers()) {
        if (teamId.equals(user.getTeamId())) {
          if (usersCached.containsKey(user.getId())) {
            var cached = usersCached.get(user.getId());
            if (cached.isEligible() != isEligible(user)) {
              cached.setEligible(isEligible(user));
              userJpaDao.save(cached);
              ctx.logger.info("User {}[{}] eligibility changed to {}", user.getName(), user.getId(), isEligible(user));
            }
          } else {
            addUser(user, ctx);
          }
        }
      }
    } else {
      ctx.logger.warn("User {}[{}] tried to sync without being admin", slackUser.getUser().getName(), slackUser.getUser().getId());
    }
    return ctx.ack();
  }

  @Transactional
  public void addUser(User user, Context ctx) {
    var eligible = isEligible(user);
    var saved = userJpaDao.save(UserCached.builder()
        .teamId(user.getTeamId())
        .userId(user.getId())
        .eligible(eligible)
        .build());
    if (eligible) {
      walletJpaDao.save(Wallet.builder()
          .user(saved)
          .build());
    }
    ctx.logger.info("User {}[{}] added with eligibility to {}", user.getName(), user.getId(), isEligible(user));
  }

  public static boolean isEligible(User user) {
    return !("USLACKBOT".equals(user.getId())
            || user.isBot()
            || user.isDeleted()
            || user.isInvitedUser()
            || user.isStranger()
            || user.isRestricted()
            || user.isAppUser());
  }
}

package org.silk.slack.coin.command;

import static org.silk.slack.coin.event.MessageEventHandler.MONEY_EMOJI;

import com.slack.api.app_backend.slash_commands.response.SlashCommandResponse;
import com.slack.api.bolt.context.builtin.SlashCommandContext;
import com.slack.api.bolt.handler.builtin.SlashCommandHandler;
import com.slack.api.bolt.request.builtin.SlashCommandRequest;
import com.slack.api.bolt.response.Response;
import com.slack.api.model.block.HeaderBlock;
import com.slack.api.model.block.SectionBlock;
import com.slack.api.model.block.composition.MarkdownTextObject;
import com.slack.api.model.block.composition.PlainTextObject;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.silk.slack.coin.dao.WalletJpaDao;
import org.silk.slack.coin.dao.entity.UserCached;
import org.silk.slack.coin.dao.entity.Wallet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component("coins")
public class CoinsCommand implements SlashCommandHandler {

  @Autowired
  private WalletJpaDao walletJpaDao;

  @Override
  @Transactional(readOnly = true)
  public Response apply(SlashCommandRequest request, SlashCommandContext ctx) {
    var opt = walletJpaDao.findOne((root, q, cb) -> {
      var joinUser = root.<Wallet, UserCached>join("user", JoinType.INNER);
      return cb.and(
          cb.equal(joinUser.get("teamId"), request.getPayload().getTeamId()),
          cb.equal(joinUser.get("userId"), request.getPayload().getUserId())
      );
    });
    if (opt.isPresent()) {
      var wallet = opt.get();
      return ctx.ack(SlashCommandResponse.builder()
          .responseType("ephemeral")
          .blocks(List.of(
              HeaderBlock.builder()
                  .text(new PlainTextObject("Etat de votre wallet", null))
                  .build(),
              SectionBlock.builder()
                  .fields(List.of(
                      new MarkdownTextObject("*Reçu(s) :* %d %s"
                          .formatted(wallet.getReceivedBalance(), MONEY_EMOJI), null),
                      new MarkdownTextObject("*Disponible(s) :* %d %s"
                          .formatted(wallet.getAvailableBalance(), MONEY_EMOJI), null),
                      new MarkdownTextObject("*Donné(s) :* %d %s"
                          .formatted(wallet.getGivenBalance(), MONEY_EMOJI), null)
                  ))
                  .build()
          ))
          .build());
    } else {
      return ctx.ack();
    }
  }
}

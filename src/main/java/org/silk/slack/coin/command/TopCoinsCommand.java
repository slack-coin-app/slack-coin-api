package org.silk.slack.coin.command;

import com.slack.api.app_backend.slash_commands.response.SlashCommandResponse;
import com.slack.api.bolt.context.builtin.SlashCommandContext;
import com.slack.api.bolt.handler.builtin.SlashCommandHandler;
import com.slack.api.bolt.request.builtin.SlashCommandRequest;
import com.slack.api.bolt.response.Response;
import com.slack.api.model.User;
import com.slack.api.model.block.HeaderBlock;
import com.slack.api.model.block.LayoutBlock;
import com.slack.api.model.block.SectionBlock;
import com.slack.api.model.block.composition.MarkdownTextObject;
import com.slack.api.model.block.composition.PlainTextObject;
import com.slack.api.model.block.composition.TextObject;
import javax.persistence.criteria.JoinType;
import org.silk.slack.coin.dao.WalletJpaDao;
import org.silk.slack.coin.dao.entity.Wallet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static org.silk.slack.coin.event.MessageEventHandler.MONEY_EMOJI;

@Component("top-coins")
public class TopCoinsCommand implements SlashCommandHandler {

  @Autowired
  private WalletJpaDao walletJpaDao;

  @Override
  @Transactional(readOnly = true)
  public Response apply(SlashCommandRequest request, SlashCommandContext ctx) {
    var blocks = new ArrayList<LayoutBlock>();

    Specification<Wallet> spec = (root, q, cb) -> {
      if (Long.class != q.getResultType()) {
        // needed for pages and count queries
        root.fetch("user", JoinType.INNER);
      }
      var joinUser = root.<Wallet, User>join("user", JoinType.INNER);
      return cb.equal(joinUser.get("teamId"), request.getPayload().getTeamId());
    };
    addMostReceivedInfo(blocks, spec);
    addMostAvailableInfo(blocks, spec);
    addMostGivenInfo(blocks, spec);

    return ctx.ack(SlashCommandResponse.builder()
        .responseType("ephemeral")
        .blocks(blocks)
        .build());
  }

  private void addMostReceivedInfo(List<LayoutBlock> blocks, Specification<Wallet> spec) {
    var mostReceived = walletJpaDao.findAll(spec,
        PageRequest.of(0, 10, Sort.by(Sort.Direction.DESC, "receivedBalance")));
    if (!mostReceived.isEmpty()) {
      blocks.add(HeaderBlock.builder()
          .text(new PlainTextObject("Top %d des plus riches".formatted(mostReceived.getContent().size()), null))
          .build());
      blocks.add(SectionBlock.builder()
          .fields(mostReceived.stream()
              .map(w -> (TextObject) new MarkdownTextObject(
                  "*<@%s>:* %d %s".formatted(w.getUser().getUserId(), w.getReceivedBalance(), MONEY_EMOJI), null))
              .toList())
          .build());
    }
  }

  private void addMostAvailableInfo(List<LayoutBlock> blocks, Specification<Wallet> spec) {
    var mostAvailable = walletJpaDao.findAll(spec,
        PageRequest.of(0, 10, Sort.by(Sort.Direction.DESC, "availableBalance")));
    if (!mostAvailable.isEmpty()) {
      blocks.add(HeaderBlock.builder()
          .text(new PlainTextObject("Top %d des gens à corrompre".formatted(mostAvailable.getContent().size()), null))
          .build());
      blocks.add(SectionBlock.builder()
          .fields(mostAvailable.stream()
              .map(w -> (TextObject) new MarkdownTextObject(
                  "*<@%s>:* %d %s".formatted(w.getUser().getUserId(), w.getAvailableBalance(), MONEY_EMOJI), null))
              .toList())
          .build());
    }
  }

  private void addMostGivenInfo(List<LayoutBlock> blocks, Specification<Wallet> spec) {
    var mostGiven = walletJpaDao.findAll(spec,
        PageRequest.of(0, 10, Sort.by(Sort.Direction.DESC, "givenBalance")));
    if (!mostGiven.isEmpty()) {
      blocks.add(HeaderBlock.builder()
          .text(new PlainTextObject("Top %d des plus généreux".formatted(mostGiven.getContent().size()), null))
          .build());
      blocks.add(SectionBlock.builder()
          .fields(mostGiven.stream()
              .map(w -> (TextObject) new MarkdownTextObject(
                  "*<@%s>:* %d %s".formatted(w.getUser().getUserId(), w.getGivenBalance(), MONEY_EMOJI), null))
              .toList())
          .build());
    }
  }
}

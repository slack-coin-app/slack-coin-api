package org.silk.slack.coin.command;

import com.slack.api.bolt.context.builtin.SlashCommandContext;
import com.slack.api.bolt.handler.builtin.SlashCommandHandler;
import com.slack.api.bolt.request.builtin.SlashCommandRequest;
import com.slack.api.bolt.response.Response;
import com.slack.api.methods.SlackApiException;
import com.slack.api.methods.request.chat.ChatPostMessageRequest;
import com.slack.api.methods.request.files.FilesUploadRequest;
import com.slack.api.methods.request.users.UsersInfoRequest;
import com.slack.api.methods.request.users.UsersListRequest;
import com.slack.api.model.User;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import javax.persistence.criteria.JoinType;
import org.silk.slack.coin.dao.WalletJpaDao;
import org.silk.slack.coin.dao.entity.Wallet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component("export-coins")
public class ExportWalletCommand implements SlashCommandHandler {

  @Autowired
  private WalletJpaDao walletJpaDao;

  @Override
  @Transactional(readOnly = true)
  public Response apply(SlashCommandRequest request, SlashCommandContext ctx) throws SlackApiException, IOException {
    var eventUser = request.getPayload().getUserId();
    var slackUser = ctx.client().usersInfo(UsersInfoRequest.builder()
            .user(eventUser)
            .build());
    var client = ctx.client();
    if (slackUser.getUser().isAdmin()) {
      var teamId = request.getPayload().getTeamId();
      var userMap = client.usersList(UsersListRequest.builder()
                      .teamId(teamId)
                      .build())
              .getMembers()
              .stream()
              .collect(Collectors.toMap(User::getId, Function.identity()));
      var wallets = walletJpaDao.findAll((root, q, cb) -> {
        root.fetch("user", JoinType.INNER);
        var joinUser = root.<Wallet, User>join("user", JoinType.INNER);
        return cb.equal(joinUser.get("teamId"), teamId);
      });
      StringBuilder sb = new StringBuilder("Slack ID;email;Montant reçu;Montant donné;Montant restant\n");
      for (var wallet : wallets) {
        sb.append(wallet.getUser().getUserId()).append(";")
                .append(userMap.get(wallet.getUser().getUserId()).getProfile().getEmail()).append(";")
                .append(wallet.getReceivedBalance()).append(";")
                .append(wallet.getGivenBalance()).append(";")
                .append(wallet.getAvailableBalance()).append("\n");
      }
      Path p = Files.createTempFile("report_" + teamId, ".csv");
      Files.writeString(p, sb.toString());
      var fileResponse = client.filesUpload(FilesUploadRequest.builder()
              .channels(List.of(slackUser.getUser().getId()))
              .filename("Rapport_de_consommation.csv")
              .content(sb.toString())
              .build());
      if (fileResponse.isOk()) {
        if (request.getPayload().getText().contains("reset")) {
          walletJpaDao.resetCoin(teamId);
          var message = client.chatPostMessage(ChatPostMessageRequest.builder()
              .channel(slackUser.getUser().getId())
              .text("Remise à zéro des wallets terminée")
              .build());
          if (message.isOk()) {
            ctx.logger.info("Could not send reset message");
          }
        } else {
          var message = client.chatPostMessage(ChatPostMessageRequest.builder()
              .channel(slackUser.getUser().getId())
              .text("Export ok, les wallets n'ont pas été remis à zéro")
              .build());
          if (message.isOk()) {
            ctx.logger.info("Could not send export message");
          }
        }
      } else {
        ctx.logger.info("Could not upload file");
      }
    } else {
      ctx.logger.warn("User {}[{}] tried to get summary without being admin", slackUser.getUser().getName(),
          slackUser.getUser().getId());
    }
    return ctx.ack();
  }
}

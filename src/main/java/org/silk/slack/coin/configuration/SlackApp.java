package org.silk.slack.coin.configuration;

import com.slack.api.bolt.App;
import com.slack.api.bolt.handler.builtin.SlashCommandHandler;
import com.slack.api.model.event.Event;
import lombok.extern.slf4j.Slf4j;
import org.silk.slack.coin.event.EventHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.List;
import java.util.Map;

@Slf4j
@Configuration
@EnableScheduling
public class SlackApp {
  @Bean
  public <E extends Event> App initSlackApp(@Autowired Map<String, SlashCommandHandler> commandExecutors,
                          @Autowired List<EventHandler<E>> eventHandlers) {
    App app = new App();
    for(var commandExecutor: commandExecutors.entrySet()) {
      app.command("/" + commandExecutor.getKey(), commandExecutor.getValue());
    }
    for (var eventHandler: eventHandlers) {
      app.event(eventHandler.applicableClass(), eventHandler);
    }
    return app;
  }
}

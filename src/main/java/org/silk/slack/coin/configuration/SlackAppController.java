package org.silk.slack.coin.configuration;

import com.slack.api.bolt.App;
import com.slack.api.bolt.servlet.SlackAppServlet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.annotation.WebServlet;

@Component
@WebServlet("/slack/events")
public class SlackAppController extends SlackAppServlet {
  public SlackAppController(@Autowired App app) {
    super(app);
  }
}
